﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceFinal : MonoBehaviour
{

	public Text textThrown;
	public Text textDestroyed;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		textThrown.text = "Number of Stones: " + GameManager.currentNumberStonesThrown;
		textDestroyed.text = "Destroyed: " + GameManager.currentNumberDestroyedStones;
	}

	public void Click(String mode) {
		if (mode.Equals("reset")) {
			Application.LoadLevel("stoneGame");
			GameManager.currentNumberDestroyedStones = 0;
			GameManager.currentNumberStonesThrown = 0;
		}
		else {
			Application.LoadLevel("Awake");
		}
		
		
	}
}
