﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stone : MonoBehaviour {

	public GameObject explosion;
	public AudioSource sound;
	public AudioClip bornAudio;
	public AudioClip deadAudio;
	
	private const float yDie = -30.0f;
	
	// Use this for initialization
	void Start () {
		sound.PlayOneShot(bornAudio);
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y < yDie) {
			Destroy(gameObject);
		}
	}

	private void OnMouseDown() {

		sound.PlayOneShot(deadAudio);

		Instantiate(explosion, transform.position, Quaternion.identity);
		GetComponent<MeshRenderer>().enabled = false;
		Destroy(gameObject,2f);
		
		if (gameObject.CompareTag("Red")) {
			GameManager.currentNumberDestroyedStones--;
			
		} else if (gameObject.CompareTag("Grey")) {
			GameManager.currentNumberDestroyedStones++;
		}
		else {
			GameManager.currentNumberDestroyedStones += 2;
		}
	}
}
